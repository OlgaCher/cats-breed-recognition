import argparse

from keras import backend as K
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential
from keras.optimizers import RMSprop
from keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
from keras.utils import np_utils

K.set_image_dim_ordering('th')

import numpy as np
import os
import matplotlib.pyplot as plt

from CNNTrain.BaseNeuralNetwork import NeuralNetwork


# ===========================================
# модуль обучения сверточной нейронной сети
# содержит актуальный вариант архитектуры
# запускается из командной строки
# example run: python CNNTrain/CNN.py recognition -imagepath resource/data/test/persian/1.jpg -modelpath model.h5 -labelpath label.txt

# positional arguments:
#   {Train,r,Recognition,recognition,t,train}   Train or Recognition
#
# optional arguments:
#   -datapath DATAPATH              path to train and test folder
#   -savemodelpath SAVEMODELPATH    path to saved model (*.h5)
#   -savelabelpath SAVELABELPATH    path to saved label (*.txt)
#   -epochs EPOCHS                  epochs number
#   -imagesize IMAGESIZE            image size
#   -imagepath IMAGEPATH            path to image
#   -modelpath MODELPATH            path to model
#   -labelpath LABELPATH            path to labels


# ===========================================

class CNN(NeuralNetwork):
    def __init__(self, outputCount=3, targetSize=224, batchSize=32):

        self.targetSize = targetSize;
        self.batchSize = batchSize;

        self.model = Sequential()
        self.labels = []

        self.outputCount = outputCount

        self.initDefaultModel();

    def initDefaultModel(self):
        self.model.add(Conv2D(8, (3, 3), input_shape=(3, self.targetSize, self.targetSize)))
        self.model.add(Activation('elu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))

        self.model.add(Conv2D(16, (3, 3)))
        self.model.add(Activation('elu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.2))

        self.model.add(Conv2D(32, (3, 3)))
        self.model.add(Activation('elu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.2))

        self.model.add(Conv2D(64, (3, 3)))
        self.model.add(Activation('elu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.2))

        self.model.add(Conv2D(128, (3, 3)))
        self.model.add(Activation('elu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.2))

        self.model.add(Flatten())
        self.model.add(Dense(128))
        self.model.add(Activation('elu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(3))
        self.model.add(Activation('sigmoid'))

        rms = RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)

        self.model.compile(loss='categorical_crossentropy',
                           optimizer=rms,
                           metrics=['accuracy'])

    def trainModel(self, trainPath,
                   epochs=10):
        maxIm = 0  # максимальное количество картинок в папке
        for dir in os.listdir(trainPath):
            if len(os.listdir(trainPath + "/" + dir)) > maxIm:
                maxIm = len(os.listdir(trainPath + "/" + dir))
            # заодно запись названий классов
            self.labels.append(dir);

        # запись картинок в массив с чередованием по классам
        ims = []
        lbs = []
        for index in range(maxIm):
            numberClass = 0
            for dir in os.listdir(trainPath):
                if index < len(os.listdir(trainPath + "/" + dir)):
                    img = load_img(trainPath + "/" + dir + "/" + os.listdir(trainPath + "/" + dir)[index],
                                   target_size=(self.targetSize, self.targetSize))
                    imgArray = img_to_array(img)
                    ims.append(imgArray)
                    lbs.append(numberClass)
                numberClass = numberClass + 1

        border = int(len(ims) * 0.1)
        images_train = ims[border:]
        images_val = ims[:border]
        labels_train = lbs[border:]
        labels_val = lbs[:border]

        x_train = np.array(images_train)
        y_train = np.array(labels_train)
        x_val = np.array(images_val)
        y_val = np.array(labels_val)
        y_train = np_utils.to_categorical(y_train)
        y_val = np_utils.to_categorical(y_val)

        del ims
        del lbs
        del images_train
        del images_val
        del labels_train
        del labels_val

        datagen = ImageDataGenerator(
            rescale=1. / 255,
            rotation_range=20,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True)

        history = self.model.fit_generator(datagen.flow(x_train, y_train, batch_size=self.batchSize),
                                           validation_data=(x_val, y_val),
                                           epochs=epochs, steps_per_epoch=len(x_train) * 3 // self.batchSize,
                                           validation_steps=len(x_val) * 3 // self.batchSize, verbose=2)
        return history

    def visual_train(self, history):
        fig, ax = plt.subplots()
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'], linestyle="--")
        ax.grid()
        plt.xticks(np.arange(0, 50, 5))
        plt.yticks(np.arange(0, max(history.history['val_loss']) + 1, 1.0))
        plt.title('Model loss')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Test'], loc='upper left')
        plt.show()

    def recognitionImage(self, pathToImage, imageSize, callback=None):
        img = load_img(pathToImage, target_size=(imageSize, imageSize))
        imgArray = img_to_array(img) / 255.0
        imgArray = np.expand_dims(imgArray, axis=0)
        p_class = self.model.predict_classes(np.asarray(imgArray))[0]
        if callback != None:
            callback(self.labels[p_class])
        else:
            return p_class, self.labels[p_class]

    def recognitionImageProbability(self, pathToImage, imageSize, callback=None):
        img = load_img(pathToImage, target_size=(imageSize, imageSize))
        imgArray = img_to_array(img) / 255.0
        imgArray = np.expand_dims(imgArray, axis=0)
        p = self.model.predict(np.asarray(imgArray))[0]
        if callback != None:
            callback(p)
        else:
            return p


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('func', type=str, help='Train or Recognition',
                        choices={'train', 'Train', 't', 'recognition', 'Recognition', 'r'})
    parser.add_argument('-datapath', type=str, default="resource/data", help='path to train and test folder')
    parser.add_argument('-savemodelpath', type=str, default="model.h5", help='path to saved model (*.h5)')
    parser.add_argument('-savelabelpath', type=str, default="label.txt", help='path to saved label (*.txt)')
    parser.add_argument('-epochs', type=int, default=50, help='epochs number')
    parser.add_argument('-imagesize', type=int, default=224, help='image size')
    parser.add_argument('-imagepath', type=str, help='path to image')
    parser.add_argument('-modelpath', type=str, help='path to model')
    parser.add_argument('-labelpath', type=str, help='path to labels')

    args = parser.parse_args()
    cnn = CNN()

    if (args.func in ['train', 'Train', 't']):
        history = cnn.trainModel(args.datapath + "/train", args.epochs)
        cnn.saveModel(args.savemodelpath)
        with open(args.savelabelpath, 'w', encoding="utf-8") as file:
            for label in cnn.labels:
                file.write(label+'\n')
        cnn.visual_train(history)
        for path in os.listdir(args.datapath + "/test"):
            pred = [0] * 3;
            for i in os.listdir(args.datapath + "/test/" + path):
                cl = cnn.recognitionImage(args.datapath + "/test/" + path + "/" + i, cnn.targetSize)
                pred[cl] = pred[cl] + 1
            print(pred)

    else:
        cnn.loadModel(args.modelpath)
        with open(args.labelpath, 'r', encoding="utf-8") as file:
            cnn.labels = [row.strip() for row in file]
        print(cnn.recognitionImage(args.imagepath, cnn.targetSize))
