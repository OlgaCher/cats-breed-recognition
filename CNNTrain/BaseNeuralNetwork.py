from keras.models import Sequential
from keras.models import load_model, model_from_json

class NeuralNetwork():
    def __init__(self):
        self.model = Sequential()

    def loadModel(self, path):
        self.model = load_model(path)

    def saveModel(self, path):
        self.model.save(path)

    def loadWeights(self, path):
        self.model.load_weights(path, by_name=True)

    def saveWeights(self, path):
        self.model.save_weights(path)

    def loadArchitecture(self, path):
        json_file = open(path, "r")
        json_model = json_file.read()
        json_file.close()
        self.model = model_from_json(json_model)

    def saveArchitecture(self, path):
        json_model = self.model.to_json()
        json_file = open(path,"w")
        json_file.write(json_model)
        json_file.close()