from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QFont, QPixmap, QIcon
from PyQt5.QtWidgets import QLabel, QWidget, QGridLayout, QPushButton, QCheckBox, QVBoxLayout
from win32api import GetSystemMetrics


#   =====================================================
#     0       1       2       3
# 0   изображение   панель кнопок
# 1   изображение   панель кнопок
# 2                 панель кнопок
# 3   результат     сообщения
#   =====================================================

def initUI(mainWindow):
    mainWindow.setWindowTitle('Распознавание образов')
    mainWindow.setWindowIcon(QIcon('../resource/img/icon.jpg'))

    window = QWidget()

    # разметка
    layout = QGridLayout(mainWindow)

    layout.setColumnStretch(0, 3)
    layout.setColumnStretch(1, 3)
    layout.setColumnStretch(2, 3)
    layout.setColumnStretch(3, 2)
    layout.setColumnStretch(4, 2)

    window.setLayout(layout)
    mainWindow.setCentralWidget(window)

    mainWindow.pixmap_label = QLabel(mainWindow)
    layout.addWidget(mainWindow.pixmap_label, 0, 0, 2, 2)
    image = QPixmap("/resource/data/test/persian/1.jpg")
    image = image.scaled(GetSystemMetrics(0) * 3 // 5, GetSystemMetrics(1) * 3 // 5, Qt.KeepAspectRatio)
    mainWindow.pixmap_label.setPixmap(image)

    bPanel = QWidget()
    bPanel.setMaximumWidth(300)
    bPanel.setContentsMargins(0, 0, 0, 0)
    vLay = QVBoxLayout()
    vLay.setContentsMargins(0, 0, 0, 0)
    bPanel.setLayout(vLay)
    layout.addWidget(bPanel, 0, 2, 3, 2)

    btnLoadImage = addButton(mainWindow, 'Загрузить изображение', mainWindow.loadImage)
    vLay.addWidget(btnLoadImage)

    buttonPanel = addButton(mainWindow, 'Распознать изображение', mainWindow.recognitionImage)
    vLay.addWidget(buttonPanel)

    cb1 = QCheckBox('С определением наличия объекта на фотографии')

    cb1.stateChanged.connect(mainWindow.changeCheck1)
    vLay.addWidget(cb1)
    cb2 = QCheckBox('С применением кадрирования')
    cb2.stateChanged.connect(mainWindow.changeCheck2)
    vLay.addWidget(cb2)

    buttonHelp = addButton(mainWindow, 'О программе', mainWindow.openHelp)
    vLay.addWidget(buttonHelp)

    mainWindow.result = QLabel("", mainWindow)
    mainWindow.result.setFont(QFont("Arial", 16, QFont.Bold))
    layout.addWidget(mainWindow.result, 3, 0, 4, 2)

    mainWindow.btnUpdate = addButton(mainWindow, 'Обновить', mainWindow.updateModel)
    mainWindow.btnUpdate.setHidden(not mainWindow.needUpdate)
    vLay.addWidget(mainWindow.btnUpdate)

    mainWindow.messages = QLabel("", mainWindow)
    mainWindow.messages.setFont(QFont("Arial", 12))
    layout.addWidget(mainWindow.messages, 3, 2, 4, 4)

    mainWindow.showMaximized()


def addButton(self, title, operation, icon=None,  btnSize=(150, 30), iconSize=(25, 25)):
    btn = QPushButton(title, self)
    btn.setMinimumHeight(25)
    if icon != None:
        btn.setIcon(QIcon(icon))
        btn.setIconSize(QSize(iconSize[0], iconSize[1]))
    btn.setContentsMargins(0, 0, 0, 0)
    btn.clicked.connect(operation)

    return btn
