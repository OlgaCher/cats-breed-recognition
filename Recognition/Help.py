from PyQt5 import QtCore
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QMainWindow, QWidget, QLabel, QLineEdit, QPushButton, QVBoxLayout, QPlainTextEdit, QTextEdit


class HelpWindow(QMainWindow):

    def __init__(self, parent=None, callback=None):
        super().__init__(parent)

        self.callback = callback

        self.initUI()

    def initUI(self):
        self.setWindowTitle('Help')

        window = QWidget()

        layout = QVBoxLayout(self)

        window.setLayout(layout)
        self.setCentralWidget(window)

        plainTextEdit = QPlainTextEdit(self)
        plainTextEdit.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        plainTextEdit.setFont(QFont("Times New Roman", 14))
        with open('../resource/help.txt', 'r', encoding="utf-8") as file:
            plainTextEdit.setPlainText(file.read())
        layout.addWidget(plainTextEdit)