import numpy as np
import tensorflow as tf
from PIL import Image
from PIL import ImageFile

from object_detection.utils import label_map_util
from object_detection.utils import ops as utils_ops

ImageFile.LOAD_TRUNCATED_IMAGES = True


# ================================================================
# поиск на изображении кошки
# включает как опредление ее нахождения, так и обрезку фотографии
# ================================================================

class preRecognition():
    def __init__(self):
        # Загружаем предварительно обученную модель в оперативную память и создаем на ее основе tf.Graph()
        # Путь к файлу с моделью
        model_file_name = '../resource/ssd_resnet50/frozen_inference_graph.pb'
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(model_file_name, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        # Загружаем метки классов
        # Путь к файлу с метками классов в каталоге с моделями TensorFlow
        label_map_file_name = '../resource/ssd_resnet50/mscoco_label_map.pbtxt'
        label_map = label_map_util.load_labelmap(label_map_file_name)
        categories = label_map_util.convert_label_map_to_categories(label_map,
                                                                    max_num_classes=90,
                                                                    use_display_name=True)
        # словарь, который содержит номера классов и соответствующие им названия объектов
        self.category_index = label_map_util.create_category_index(categories)

        # Используем модель (граф TensorFlow), которую ранее загрузили в память
        with self.detection_graph.as_default():

            self.sess = tf.Session()

            # Готовим операции и входные данные
            ops = tf.get_default_graph().get_operations()
            all_tensor_names = {output.name for op in ops for output in op.outputs}
            self.tensor_dict = {}
            for key in [
                'num_detections', 'detection_boxes', 'detection_scores',
                'detection_classes', 'detection_masks'
            ]:
                tensor_name = key + ':0'
                if tensor_name in all_tensor_names:
                    self.tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(tensor_name)

            if 'detection_masks' in self.tensor_dict:
                detection_boxes = tf.squeeze(self.tensor_dict['detection_boxes'], [0])
                detection_masks = tf.squeeze(self.tensor_dict['detection_masks'], [0])
                real_num_detection = tf.cast(self.tensor_dict['num_detections'][0], tf.int32)
                detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
                detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
                detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(detection_masks, detection_boxes,
                                                                                      210, 210)
                detection_masks_reframed = tf.cast(tf.greater(detection_masks_reframed, 0.5), tf.uint8)
                self.tensor_dict['detection_masks'] = tf.expand_dims(detection_masks_reframed, 0)

    def catOrNotCat(self, pathToImage, callback=None):
        # Загружаем изображение для поиска объектов
        image_file_name = pathToImage
        image = Image.open(image_file_name)
        (im_width, im_height) = image.size
        image_np = np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)

        with self.detection_graph.as_default():

            # Запуск поиска объектов на изображении
            image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

            output_dict = self.sess.run(self.tensor_dict,
                                        feed_dict={image_tensor: np.expand_dims(image_np, 0)})

            # Преобразуем выходные тензоры типа float32 в нужный формат
            output_dict['num_detections'] = int(output_dict['num_detections'][0])
            output_dict['detection_classes'] = output_dict['detection_classes'][0].astype(np.uint8)
            output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
            output_dict['detection_scores'] = output_dict['detection_scores'][0]
            if 'detection_masks' in output_dict:
                output_dict['detection_masks'] = output_dict['detection_masks'][0]

            for cl in output_dict['detection_classes']:
                if cl == 17:
                    if callback != None:
                        callback(True)
                        return
                    else:
                        return True
            if callback != None:
                callback(False)
                return
            else:
                return False

    def cutCat(self, pathToImage, callback=None):
        # Загружаем изображение для поиска объектов
        image_file_name = pathToImage
        image = Image.open(image_file_name)
        (im_width, im_height) = image.size
        image_np = np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)

        with self.detection_graph.as_default():

            # Запуск поиска объектов на изображении
            image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

            output_dict = self.sess.run(self.tensor_dict,
                                        feed_dict={image_tensor: np.expand_dims(image_np, 0)})

            # Преобразуем выходные тензоры типа float32 в нужный формат
            output_dict['num_detections'] = int(output_dict['num_detections'][0])
            output_dict['detection_classes'] = output_dict['detection_classes'][0].astype(np.uint8)
            output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
            output_dict['detection_scores'] = output_dict['detection_scores'][0]
            if 'detection_masks' in output_dict:
                output_dict['detection_masks'] = output_dict['detection_masks'][0]

            path = 'temp.jpg'
            for index in range(len(output_dict['detection_classes'])):
                if output_dict['detection_classes'][index] == 17:
                    (left, right, top, bottom) = (output_dict['detection_boxes'][index][1] * im_width,
                                                  output_dict['detection_boxes'][index][3] * im_width,
                                                  output_dict['detection_boxes'][index][0] * im_height,
                                                  output_dict['detection_boxes'][index][2] * im_height)
                    img3 = image.crop((left, top, right, bottom))
                    img3.save(path)
                    if callback != None:
                        callback(path)
                        return
                    else:
                        return path
            if callback != None:
                callback(None)
                return
            else:
                return None


if __name__ == '__main__':
    pr = preRecognition()
    # print(pr.catOrNotCat("data/test/persian/1.jpg"))
    print(pr.cutCat("F:/документы/Диплом/Cat's breed recognition/Recognition/1.jpg"))
