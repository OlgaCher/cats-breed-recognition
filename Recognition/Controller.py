import base64
import sys
import urllib.request
from threading import Thread

from PyQt5 import QtGui
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QStyleFactory, QMainWindow, QFileDialog
from win32api import GetSystemMetrics

from CNNTrain.CNN import CNN
from Recognition.Interface import initUI
from Recognition.TensorflowRecognition import preRecognition
from Recognition.Help import HelpWindow

_api_url = 'http://api.bitbucket.org/2.0/'
_login = 'cheremisinova8@gmail.com'
_password = 'yaBlokoigruSha'
_username = 'OlgaCher'
_repository = 'cats-breed-recognition'


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.pathToImage = ""
        self.pathToTrainImages = ""
        self.epochs = 0;
        self.history = None
        self.flag1 = False;
        self.flag2 = False;
        self.btnUpdate = None
        self.cnn = CNN()
        self.cnn.loadModel("../model_release.h5")
        self.imageSize = self.cnn.model.get_config().get('layers')[0].get('config').get('batch_input_shape')[2]
        self.classCount = self.cnn.model.get_config().get('layers')[0].get('config').get('batch_input_shape')[1]
        self.labels = []
        with open("../labels_release.txt", 'r', encoding="utf-8") as file:
            self.labels = [row.strip() for row in file]
        self._newVersion = ""
        self.needUpdate = self.checkUpdateModel()
        self.preRecognition = preRecognition()
        initUI(self)

    def checkUpdateModel(self):
        query_url = _api_url + 'repositories/%s/%s/src/master/version.txt' % (_username, _repository)
        req = urllib.request.Request(query_url)

        credentials = ('%s:%s' % (_login, _password))
        encoded_credentials = base64.b64encode(credentials.encode('ascii'))
        req.add_header('Authorization', 'Basic %s' % encoded_credentials.decode("ascii"))

        try:
            with urllib.request.urlopen(req) as response, open('../version.txt', 'r') as out_file:
                serverFile = response.read().decode("utf-8")
                actualVersion = ""
                for line in serverFile.splitlines():
                    if line.find('model_version:') == 0:
                        actualVersion = line[15:]

                currentVersion = ""
                localFile = out_file.readlines()
                for line in localFile:
                    if line.find('model_version:') == 0:
                        currentVersion = line[15:].rstrip()

                if (actualVersion == currentVersion):
                    return False
                else:
                    self._newVersion = actualVersion
                    return True
        except Exception as e:
            return False

    def updateModel(self):
        self.errorMessage('Обновление программы...', 'green')
        p = Thread(target=self.startUpdate)
        p.start()

    def startUpdate(self):
        credentials = ('%s:%s' % (_login, _password))
        encoded_credentials = base64.b64encode(credentials.encode('ascii'))

        label_url = _api_url + 'repositories/%s/%s/src/master/labels_release.txt' % (_username, _repository)
        req = urllib.request.Request(label_url)
        req.add_header('Authorization', 'Basic %s' % encoded_credentials.decode("ascii"))

        try:
            with urllib.request.urlopen(req) as response, open('../labels_release.txt', 'w',
                                                               encoding="utf-8") as out_file:
                data = response.read().decode("utf-8")
                out_file.write(data)

            model_url = _api_url + 'repositories/%s/%s/src/master/model_release.h5' % (_username, _repository)
            req = urllib.request.Request(model_url)
            req.add_header('Authorization', 'Basic %s' % encoded_credentials.decode("ascii"))

            with urllib.request.urlopen(req) as response, open('../model_release.h5', 'wb') as out_file:
                data = response.read()
                out_file.write(data)

            text = ""
            with open('../version.txt', 'r') as version_file:
                for line in version_file:
                    if line.find('model_version:') == 0:
                        newline = line[:15]
                        newline = newline + self._newVersion + '\n'
                        text += newline
                    else:
                        text += line

            with open('../version.txt', 'w') as version_file:
                version_file.write(text)

            self.btnUpdate.setHidden(True)
        except:
            self.errorMessage('Не удалось обновить', 'red')
        else:
            self.errorMessage('')

    def loadImage(self):
        fd = QFileDialog()
        self.pathToImage = fd.getOpenFileName(self, 'Open file', '', "Image files (*.jpg *.jpeg *.png)")[0]
        image = QPixmap(self.pathToImage)
        image = image.scaled(GetSystemMetrics(0) // 2, GetSystemMetrics(1) // 2, Qt.KeepAspectRatio)
        self.pixmap_label.setPixmap(image)
        self.result.setText("")

    def openLoadDialog(self):
        fd = QFileDialog()
        fname = fd.getOpenFileName(self, 'Open file', '', "CNN Model (*.h5)")[0]
        if fname:
            self.cnn.loadModel(fname)
            self.statusLabel.setPixmap(self.loadStatusIcon("img/tickregular_106284.png"))
            config = self.cnn.model.get_config()
            self.configField.insertPlainText(self.parser.configToString(config))
        else:
            self.statusLabel.setPixmap(self.loadStatusIcon("img/dangerregular_106328.png"))

    def recognitionImage(self):
        self.result.setText('')
        if self.pathToImage:
            self.errorMessage('Распознавание изображения...', 'green')
            try:
                self.cnn.model._make_predict_function()
                if self.flag2:
                    p = Thread(target=self.preRecognition.cutCat, args=(self.pathToImage, self.haveCutPreResult))
                    p.start()
                elif self.flag1:
                    p = Thread(target=self.preRecognition.catOrNotCat, args=(self.pathToImage, self.haveIsPreResult))
                    p.start()
                else:
                    p = Thread(target=self.cnn.recognitionImageProbability,
                               args=(self.pathToImage, self.imageSize, self.haveResult))
                    p.start()
            except:
                self.errorMessage('Ошибка распознавания', 'red')

    def haveCutPreResult(self, path):
        if path != None:
            p = Thread(target=self.cnn.recognitionImageProbability, args=(path, self.imageSize, self.haveResult))
            p.start()
        else:
            self.errorMessage('')
            if self.flag1:
                self.result.setText('На фотографии нет кошки.')
            else:
                self.result.setText('Ошибка кадрирования: кошка не найдена.\nПопробуйте распознать без кадрирования.')

    def haveIsPreResult(self, isCat):
        if isCat == True:
            p = Thread(target=self.cnn.recognitionImageProbability,
                       args=(self.pathToImage, self.imageSize, self.haveResult))
            p.start()
        else:
            self.errorMessage('')
            self.result.setText('На фотографии нет кошки.')

    def haveResult(self, catClass):
        self.errorMessage('')
        text = 'Порода: '
        maxProbIndex = 0
        for i in range(1, len(catClass)):
            if catClass[i] > catClass[maxProbIndex]:
                maxProbIndex = i
        text += self.labels[maxProbIndex]
        text = text + ' (' + str(catClass[maxProbIndex]) + ')'
        text += '\nТакже возможно: '
        probIndex = 0
        for i in range(0, len(catClass)):
            if (i != maxProbIndex):
                probIndex = i
                break
        for i in range(0, len(catClass)):
            if (catClass[i] >= catClass[probIndex]) and (i != maxProbIndex):
                probIndex = i
        text += self.labels[probIndex]
        text = text + ' (' + str(catClass[probIndex]) + ')'
        self.result.setText(text)

    def loadStatusIcon(self, path):
        image = QPixmap(path)
        image = image.scaled(20, 20, Qt.KeepAspectRatio)
        return image

    def openDialogForTrain(self):
        fd = QFileDialog()
        self.pathToTrainImages = fd.getExistingDirectory(self, "Select Directory")

    def changeCheck1(self, state):
        if state == Qt.Checked:
            self.flag1 = True
        else:
            self.flag1 = False

    def changeCheck2(self, state):
        if state == Qt.Checked:
            self.flag2 = True
        else:
            self.flag2 = False

    def errorMessage(self, text, color=None):
        if color != None:
            pal = mainWindow.messages.palette()
            pal.setColor(QtGui.QPalette.WindowText, QtGui.QColor(color))
            mainWindow.messages.setPalette(pal)
        self.messages.setText(text)

    def openHelp(self):
        self.help = HelpWindow(self)
        self.help.setWindowModality(Qt.WindowModal)
        self.help.showMaximized()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle(QStyleFactory.create('Fusion'))
    mainWindow = MainWindow()
    sys.exit(app.exec_())
