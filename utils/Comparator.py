import os
from PIL import Image, ImageChops

# =============================================================
# поиск одинаковых картинок в двух папках и вывод их названий
# проверка, что в тестовом наборе нет картинок из обучающего
# =============================================================

pathTrain = "data/train/Сфинкс"
pathTest = "data/test/sphynx"

def equal(im1, im2):
  return ImageChops.difference(im1, im2).getbbox() is None

for testIm in os.listdir(pathTest):
    testFile = Image.open(pathTest+"/"+testIm)
    for trainIm in os.listdir(pathTrain):
        trainFile = Image.open(pathTrain + "/" + trainIm)
        eq = equal(testFile.convert('RGBA'), trainFile.convert('RGBA'))
        if eq:
            print(testIm)
            break
