import urllib.request
from urllib.error import HTTPError, URLError
from socket import error as SocketError
import re
import pathlib

# =============================================================
# скачивает картинки с сайта ImageNet
# =============================================================

url = "http://www.image-net.org/api/text/imagenet.synset.geturls?wnid=n02123478"    # url со списком фоток
valide_extensions = [".jpg", ".gif", ".jpeg"]                                       # расширения файлов, которые буду скачиваться
folder_name = "data/angora"                                                     # имя папки, куда будут фотки сохраняться


def get_next_name(name_to_next, extension):
    pathlib.Path(folder_name).mkdir(exist_ok=True)
    return folder_name + "/" + str(name_to_next) + extension


def main():
    counter_img = 0
    counter = 0
    links = urllib.request.urlopen(url).read().decode("utf-8").split("\r\n")
    for link in links:
        counter += 1
        extension = link[link.rfind('.'):len(link)].lower()
        if extension in valide_extensions:
            try:
                img = urllib.request.urlopen(link).read()
            except HTTPError:
                print(str(counter) + " of " + str(len(links)) + " failed")
                continue
            except URLError:
                print(str(counter) + " of " + str(len(links)) + " failed")
                continue
            except SocketError:
                print(str(counter) + " of " + str(len(links)) + " failed")
                continue
            except Exception:
                print(str(counter) + " of " + str(len(links)) + " exception")
                continue
            if (len(img) <= 2200):
                print(str(counter) + " of " + str(len(links)) + " failed")
                continue
            out = open(get_next_name(counter_img, extension), "wb")
            out.write(img)
            out.close
            counter_img += 1
        print(str(counter) + " of " + str(len(links)) + " successfully")


if __name__ == "__main__":
    main()
