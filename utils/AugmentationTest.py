from keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.optimizers import RMSprop
from keras.utils import np_utils
from keras.callbacks import LambdaCallback, EarlyStopping

from keras import backend as K
K.set_image_dim_ordering('th')

import numpy as np
import os

# =====================================================
# аугментация картинок и сохранение результата в папку
# проверка работы ImageDataGenerator
# =====================================================

def trainModel(trainPath):
    numberClass = 0;
    ims = []
    lbs = []
    for im in os.listdir(trainPath):
        img = load_img(trainPath+"/"+ im, target_size=(150, 150))
        imgArray = img_to_array(img)
        ims.append(imgArray)
        lbs.append(numberClass)

    images_train = ims
    labels_train = lbs

    x_train = np.array(images_train)
    y_train = np.array(labels_train)
    y_train = np_utils.to_categorical(y_train)

    datagen = ImageDataGenerator(
        rescale=1. / 255,
        rotation_range=20,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)
    # validation_split=0.2)

    e = 0
    for batch in datagen.flow(x_train, y_train, batch_size=1,
                 save_to_dir="augmImg", save_prefix='cat', save_format='jpeg'):
        e += 1
        if e == 14:
            break;

if __name__ == '__main__':
    trainModel("data/aug")