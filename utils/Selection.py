from keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.optimizers import RMSprop
from keras.utils import np_utils
from keras.callbacks import LambdaCallback, EarlyStopping

from keras import backend as K
K.set_image_dim_ordering('th')

import numpy as np
import os

from CNNTrain.BaseNeuralNetwork import NeuralNetwork
import matplotlib.pyplot as plt

# =====================================================================
# модуль для экспериментального моделирования - подбора параметров CNN
# =====================================================================

# версия для обучения нейросети для андроида
class CNN(NeuralNetwork):
    def __init__(self, targetSize = 224, batchSize = 32):

        self.targetSize = targetSize;
        self.batchSize = batchSize;

        self.model = Sequential()
        self.labels = []

        self.initDefaultModel();


    def initDefaultModel(self):
        self.model.add(Conv2D(8, (3, 3), input_shape=(3, self.targetSize, self.targetSize)))
        self.model.add(Activation('elu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))

        self.model.add(Conv2D(16, (3, 3)))
        self.model.add(Activation('elu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.2))

        self.model.add(Conv2D(32, (3, 3)))
        self.model.add(Activation('elu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.2))

        self.model.add(Conv2D(64, (3, 3)))
        self.model.add(Activation('elu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.2))

        self.model.add(Conv2D(128, (3, 3)))
        self.model.add(Activation('elu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.2))

        self.model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
        self.model.add(Dense(128))
        self.model.add(Activation('elu'))
        self.model.add(Dropout(0.5))
        # self.model.add(Dense(256))
        # self.model.add(Activation('elu'))
        # self.model.add(Dropout(0.5))
        # self.model.add(Dense(256))
        # self.model.add(Activation('elu'))
        # self.model.add(Dropout(0.5))
        self.model.add(Dense(3))
        self.model.add(Activation('sigmoid'))

        rms = RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)

        self.model.compile(loss='categorical_crossentropy',
                      optimizer=rms,
                      metrics=['accuracy'])


    def trainModel(self,trainPath,
                        epochs=10):
        # images_train = []
        # images_val = []
        # labels_train = [];
        # labels_val = [];
        # numberClass = 0;
        # for dir in os.listdir(trainPath):
        #     ims = []
        #     lbs = []
        #     for im in os.listdir(trainPath+"/"+dir):
        #         img = load_img(trainPath+"/"+dir+"/" + im, target_size=(self.targetSize, self.targetSize))
        #         imgArray = img_to_array(img)
        #         ims.append(imgArray)
        #         lbs.append(numberClass)
        #     border = int(len(ims) * 0.9)
        #     images_train = images_train + ims[:border]
        #     images_val = images_val + ims[border:]
        #     labels_train = labels_train + lbs[:border]
        #     labels_val = labels_val + lbs[border:]
        #     numberClass = numberClass + 1
        #     self.labels.append(dir)

        maxIm = 0
        for dir in os.listdir(trainPath):
            if len(os.listdir(trainPath+"/"+dir)) > maxIm:
                maxIm = len(os.listdir(trainPath+"/"+dir))

        ims = []
        lbs = []
        for index in range(maxIm):
            numberClass = 0
            for dir in os.listdir(trainPath):
                if index < len(os.listdir(trainPath+"/"+dir)):
                    img = load_img(trainPath+"/"+dir+"/" + os.listdir(trainPath+"/"+dir)[index], target_size=(self.targetSize, self.targetSize))
                    imgArray = img_to_array(img)
                    # imgArray = imgArray*2/255.0-1
                    ims.append(imgArray)
                    lbs.append(numberClass)
                numberClass = numberClass + 1


        border = int(len(ims) * 0.1)
        images_train = ims[border:]
        images_val = ims[:border]
        labels_train = lbs[border:]
        labels_val = lbs[:border]

        x_train = np.array(images_train)
        y_train = np.array(labels_train)
        x_val = np.array(images_val)
        y_val = np.array(labels_val)
        y_train = np_utils.to_categorical(y_train)
        y_val = np_utils.to_categorical(y_val)

        datagen = ImageDataGenerator(
            rescale=1. / 255,
            rotation_range=20,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True)
            # validation_split=0.2)

        earlyStopping = EarlyStopping(patience=10, restore_best_weights=True)

        history = self.model.fit_generator(datagen.flow(x_train, y_train, batch_size=self.batchSize),validation_data=(x_val, y_val),
                                  epochs=epochs, steps_per_epoch=len(x_train) * 3 // self.batchSize,
                                 validation_steps=len(x_val) * 3 // self.batchSize, verbose=2)

        # history = self.model.fit(x_train, y_train, batch_size=self.batchSize,validation_data=(x_val, y_val),
        #                           epochs=epochs, verbose=2)

        cnn.saveModel("model_b.h5")

        fig, ax = plt.subplots()
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'], linestyle="--")
        ax.grid()
        plt.xticks(np.arange(0, 50, 5))
        plt.yticks(np.arange(0, max(history.history['val_loss']) + 1, 1.0))
        plt.title('Model loss')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Test'], loc='upper left')
        plt.show()


    def recognitionImageTTA(self,pathToImage):
        self.labels = ['Персидский кот','Сиамский кот','Сфинкс']
        img = load_img(pathToImage, target_size=(self.targetSize, self.targetSize))
        imgArray = img_to_array(img)/255.0
        imgArray = np.expand_dims(imgArray, axis=0)
        predict_array = []
        p_class = self.model.predict_classes(np.asarray(imgArray))[0]
        predict_array.append(p_class)

        datagen = ImageDataGenerator(
            horizontal_flip=True)
        for batch in datagen.flow(np.asarray(imgArray), batch_size=1):
            p_class = self.model.predict_classes(batch)[0]
            predict_array.append(p_class)
            break

        datagen = ImageDataGenerator(
            zoom_range=0.1)
        for batch in datagen.flow(np.asarray(imgArray), batch_size=1):
            p_class = self.model.predict_classes(batch)[0]
            predict_array.append(p_class)
            break

        datagen = ImageDataGenerator(
            shear_range=0.1)
        for batch in datagen.flow(np.asarray(imgArray), batch_size=1):
            p_class = self.model.predict_classes(batch)[0]
            predict_array.append(p_class)
            break

        datagen = ImageDataGenerator(
            rotation_range=10)
        for batch in datagen.flow(np.asarray(imgArray), batch_size=1):
            p_class = self.model.predict_classes(batch)[0]
            predict_array.append(p_class)
            break

        return int(round(np.mean(predict_array)))


    def recognitionImage(self,pathToImage,isLabel=False,callback=None):
        self.labels = ['Персидский кот','Сиамский кот','Сфинкс']
        img = load_img(pathToImage, target_size=(self.targetSize, self.targetSize))
        imgArray = img_to_array(img)/255.0
        # imgArray = img_to_array(img)
        # imgArray = imgArray * 2 / 255.0 - 1
        imgArray = np.expand_dims(imgArray, axis=0)
        p_class = self.model.predict_classes(np.asarray(imgArray))[0]
        return p_class



if __name__ == '__main__':
    cnn = CNN()
    cnn.trainModel("cutImage/train", 38)
    # cnn.saveModel("model(test_vis).h5")
    # cnn.loadModel("model_beta.h5")
    # cnn.recognitionImage("data\\test\\persian\\2.jpg")
    #
    # cnn.recognitionImage("data\\train\\siamese\\0.jpg")
    #
    # cnn.recognitionImage("data\\train\\sphynx\\sphynx.0.jpg")

    paths = ["cutImage/test/persian/", "cutImage/test/siamese/", "cutImage/test/sphynx/"]
    # paths = ["data\\test\\hedgehog\\"]

    # preRec = preRecognition()
    #
    # for path in os.listdir("data\\test\\"):
    #     pred = [0]*4;
    #     for i in os.listdir("data\\test\\" + path):
    #         # print("data\\test\\" + path + "\\" + i)
    #         if preRec.catOrNotCat("data\\test\\" + path + "\\" + i):
    #             cl = cnn.recognitionImage("data\\test\\" + path + "\\" + i)
    #             pred[cl] = pred[cl]+1
    #         else:
    #             pred[len(pred)-1] = pred[len(pred)-1] + 1
    #     print(path)
    #     print(pred)

    for path in paths:
        pred = [0]*3;
        for i in os.listdir(path):
            cl = cnn.recognitionImage(path+i)
            pred[cl] = pred[cl]+1
        print(pred)

    # cnn.recognitionImage("data\\test\\hedgehog\\0.jpg")

    # cl = cnn.recognitionImage("data\\q.jpg", "data\\train")
    # print(cl[0])