import os
import shutil

# =====================================================
# модуль обновления файла модели НС
# актуальная версия - "../model_release.h5"
# устаревший файл перемещается в папку recource/models
# новый переименовывается в "../model_release.h5"
# обновляется файл с номером версии
# ====================================================

_newModel = "../resource/models/model0.2.0.h5"
_newVersion = "0.2.0"

if os.path.exists(_newModel) and os.path.exists("../model_release.h5"):
    version = ""
    with open('../version.txt', 'r') as version_file:
        for line in version_file:
            if line.find('model_version:') == 0:
                version = line[15:].rstrip()

    shutil.move("../model_release.h5", "../resource/models/model" + version + ".h5")

    shutil.move(_newModel, "../model_release.h5")

    text = ""
    with open('../version.txt', 'r') as version_file:
        for line in version_file:
            if line.find('model_version:') == 0:
                version = line[15:].rstrip()
                newline = line[:15]
                newline = newline + _newVersion + '\n'
                text += newline
            else:
                text += line

    with open('../version.txt', 'w') as version_file:
        version_file.write(text)

else:
    print('Проверьте правильность путей. Файла не существует.')
